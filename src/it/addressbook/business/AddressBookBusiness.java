package it.addressbook.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.jdbc.MysqlDataSource;

import it.addressbook.model.Contatto;

public class AddressBookBusiness {
	private Connection con;
	private static AddressBookBusiness abb;
	
	private AddressBookBusiness() {}
	
	public static AddressBookBusiness getInstance() {
		if(abb == null) {
			abb = new AddressBookBusiness();			
		}
		return abb;
	}
	
	private Connection getConnection() throws SQLException {
		if(con == null) {
			MysqlDataSource dataSource = new MysqlDataSource();
			dataSource.setServerName("127.0.0.1");
			dataSource.setPortNumber(3306);
			dataSource.setUser("root");
			dataSource.setPassword("root");
			dataSource.setDatabaseName("address_book");
			
			con = dataSource.getConnection();
		}
		return con;
	}
	
	public int aggiungiContatto(Contatto c) throws SQLException {
		String sql = "INSERT INTO contatti(nome, cognome, telefono) VALUES(?, ?, ?)";
		PreparedStatement ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, c.getNome());
		ps.setString(2, c.getCognome());
		ps.setString(3, c.getTelefono());
		
		ps.executeUpdate();
		
		ResultSet rs = ps.getGeneratedKeys();
		rs.next();
		
		return rs.getInt(1);
	}
	
	public List<Contatto> ricercaContatti() throws SQLException {
		String sql = "SELECT * FROM contatti";
		PreparedStatement ps = getConnection().prepareStatement(sql);
		
		ResultSet rs =  ps.executeQuery();
		
		List<Contatto> contatti = new ArrayList<Contatto>();
		while(rs.next()) {
			Contatto c = new Contatto();
			c.setId(rs.getInt(1));
			c.setNome(rs.getString(2));
			c.setCognome(rs.getString(3));
			c.setTelefono(rs.getString(4));
			
			contatti.add(c);
		}
		
		return contatti;
		
	}
	
}	
