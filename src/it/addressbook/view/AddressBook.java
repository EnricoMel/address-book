package it.addressbook.view;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

import it.addressbook.business.AddressBookBusiness;
import it.addressbook.model.Contatto;

public class AddressBook {

	private JFrame frame;
	private JTextField txtNome;
	private JTextField txtCognome;
	private JTextField txtTelefono;
	private JTable listaContatti;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddressBook window = new AddressBook();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddressBook() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1101, 772);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 30, 1065, 680);
		frame.getContentPane().add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Inserisci contatto", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(23, 42, 46, 14);
		panel.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(109, 39, 143, 20);
		panel.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblCognome = new JLabel("Cognome");
		lblCognome.setBounds(23, 87, 65, 14);
		panel.add(lblCognome);
		
		txtCognome = new JTextField();
		txtCognome.setBounds(109, 84, 143, 20);
		panel.add(txtCognome);
		txtCognome.setColumns(10);
		
		JLabel lblTelefono = new JLabel("Telefono");
		lblTelefono.setBounds(23, 131, 76, 14);
		panel.add(lblTelefono);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(109, 128, 143, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		JButton btnAggiungi = new JButton("Aggiungi");
		btnAggiungi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Contatto nuovoContatto = new Contatto();
				nuovoContatto.setCognome(txtCognome.getText());
				nuovoContatto.setNome(txtNome.getText());
				nuovoContatto.setTelefono(txtTelefono.getText());
				
				try {
					int id = AddressBookBusiness.getInstance().aggiungiContatto(nuovoContatto);
					if(id > 0) {
						JOptionPane.showMessageDialog(null, "Contatto inserito correttamente");
						txtNome.setText("");
						txtCognome.setText("");
						txtTelefono.setText("");
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnAggiungi.setBounds(23, 189, 89, 23);
		panel.add(btnAggiungi);
		
		JButton btnAnnulla = new JButton("Annulla");
		btnAnnulla.setBounds(163, 189, 89, 23);
		panel.add(btnAnnulla);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Ricerca contatti", null, panel_1, null);
		panel_1.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 39, 1029, 587);
		panel_1.add(scrollPane);
		
		listaContatti = new JTable();
		listaContatti.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "Nome", "Cognome", "Telefono"
			}
		) {
			boolean[] columnEditables = new boolean[] {
				false, true, true, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		listaContatti.getColumnModel().getColumn(0).setPreferredWidth(144);
		listaContatti.getColumnModel().getColumn(1).setPreferredWidth(143);
		listaContatti.getColumnModel().getColumn(2).setPreferredWidth(133);
		listaContatti.getColumnModel().getColumn(3).setPreferredWidth(140);
		
		DefaultTableModel dtm = (DefaultTableModel) listaContatti.getModel();
	
			try {
				List<Contatto> contatti = AddressBookBusiness.getInstance().ricercaContatti();
				for (Contatto c : contatti) {
					Vector rowData = new Vector();
					rowData.add(c.getId());
					rowData.add(c.getNome());
					rowData.add(c.getCognome());
					rowData.add(c.getTelefono());
					
					dtm.addRow(rowData);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
		scrollPane.setViewportView(listaContatti);
	}	
}
